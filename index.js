function createList(arr, parent = document.body) {
    const ul = document.createElement("ul");

    for(let i = 0; i < arr.length; i++) {
        const li = document.createElement("li");
        li.textContent = arr[i];
        ul.appendChild(li);
    }
    parent.appendChild(ul);
}


const myArray = ['Apple', 'Banana', 'Orange'];
createList(myArray);